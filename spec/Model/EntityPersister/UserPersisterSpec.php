<?php

namespace spec\App\Model\EntityPersister;

use App\Madisoft\Entity\User;
use App\Model\EntityPersister\UserPersister;
use App\Model\UserToDTOConverter;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Monolog\Logger;
use PhpSpec\ObjectBehavior;
use Prophecy\Prophet;

class UserPersisterSpec extends ObjectBehavior
{

    function let(EntityManager $em, Logger $logger, UserRepository $repo)
    {
        $this->beConstructedWith($em, $logger);
        $em->getRepository('App:UserModelDTO')->willReturn($repo);
        $em->getRepository('App:UserModelDTO')->willReturn($repo);
        $em->flush()->willReturn();
//        $repo->beConstructedWith($em);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UserPersister::class);
    }

    function it_saves_users(UserRepository $repo, User $user)
    {
        $user->getRole()->willReturn("ROLE_CUSTOMER");
        $user->getPlain()->willReturn("password");
        $user->getFirstName()->willReturn('fn');
        $user->getLastName()->willReturn('ln');
        $user->getEmail()->willReturn('email@madisoft.it');
        $repo->save(UserToDTOConverter::convert($user->getWrappedObject()))->willReturn(true);
        $this->save($user)->shouldBe($user);
    }

    function it_saves_users_fail_1(UserRepository $repo, User $user)
    {
        $user->getRole()->willReturn("ROLE_CUSTOMER");
        $user->getPlain()->willReturn(null);
        $user->getFirstName()->willReturn('fn');
        $user->getLastName()->willReturn('ln');
        $user->getEmail()->willReturn('email@madisoft.it');
        $repo->save(UserToDTOConverter::convert($user->getWrappedObject()))->willThrow(ORMException::class);
        $this->save($user)->shouldBe(null);
    }
}
