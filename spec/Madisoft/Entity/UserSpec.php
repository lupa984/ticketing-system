<?php

namespace spec\App\Madisoft\Entity;

use App\Madisoft\Entity\AbstractTicket;
use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Madisoft\Exception\SchedulerException;
use App\Madisoft\Exception\TicketException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Prophecy\Prophet;

class UserSpec extends ObjectBehavior
{
    /**
     * @var Prophet
     */
    private $prophet;

    function let()
    {
        $this->prophet = new Prophet();
    }

    function it_is_initializable()
    {
        $this->beConstructedWith(User::ROLE_CUSTOMER);
        $this->shouldHaveType(User::class);
    }

    public function it_opens_tickets()
    {
        $this->beConstructedWith(User::ROLE_CUSTOMER);

        $this->openTicket("title", "text")
            ->shouldBeAnInstanceOf(AbstractTicket::class);

        $this->shouldThrow(TicketException::class)
            ->during("openTicket", ["", "text"]);
    }

    public function it_claims_tickets()
    {
        $this->beConstructedWith(User::ROLE_ADMIN);

        $ticket  = (new User(User::ROLE_CUSTOMER))->openTicket("title", "text");

        $this->claim($ticket)
            ->shouldBeAnInstanceOf(Ticket::class);
    }

    public function it_claims_tickets_fail_1()
    {
        $this->beConstructedWith(User::ROLE_ADMIN);
        $other_admin = new User(User::ROLE_ADMIN);
        $ticket = (new User(User::ROLE_CUSTOMER))->openTicket("title", "text");
        $other_admin->claim($ticket);

        $this->shouldThrow(SchedulerException::class)
            ->during("claim", [$ticket]);
    }

    public function it_closes_tickets()
    {
        $this->beConstructedWith(User::ROLE_CUSTOMER);

        $ticket = $this->openTicket("title", "text")
            ->getWrappedObject();
        $admin = new User(User::ROLE_ADMIN);

        $admin->claim($ticket);
        $this->close($ticket)->shouldBeAnInstanceOf(Ticket::class);
    }

    public function it_assigns_tickets()
    {
        $this->beConstructedWith(User::ROLE_ADMIN, 'a', 'b', 'a@b.it');

        $other_admin = new User(User::ROLE_ADMIN, 'b', 'a', 'b@a.it');
        $customer = new User(User::ROLE_CUSTOMER);
        $ticket = $customer->openTicket("title", "text");
        $this->claim($ticket);
        $this->assign($ticket, $other_admin)->shouldBeAnInstanceOf(Ticket::class);
    }

    public function it_writes_messages()
    {
        $this->beConstructedWith(User::ROLE_ADMIN);
        $customer = new User(User::ROLE_CUSTOMER);
        $ticket = $customer->openTicket("title", "text");
        $this->claim($ticket);

        $this->writeMessage($ticket, "message text")->shouldBeAnInstanceOf(Message::class);
    }

    public function it_reads_tickets()
    {
        $this->beConstructedWith(User::ROLE_CUSTOMER);
        $admin = new User(User::ROLE_ADMIN);
        $ticket = $this->openTicket("title", "text")->getWrappedObject();
        $admin->claim($ticket);
        $this->read($ticket)->shouldHaveType(Ticket::class);
    }
}
