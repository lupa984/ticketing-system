<?php

namespace spec\App\Madisoft\Entity;

use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\User;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MessageSpec extends ObjectBehavior
{
    function let()
    {
        $admin = new User("ROLE_ADMIN");
        $customer = new User("ROLE_CUSTOMER");
        $ticket = $customer->openTicket("title", "text");
        $admin->claim($ticket);
        $this->beConstructedWith($admin, $ticket, "message text");
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Message::class);
    }
}
