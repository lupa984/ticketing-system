<?php

namespace spec\App\Madisoft\Entity;

use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Prophecy\Prophet;

class TicketSpec extends ObjectBehavior
{
    /**
     * @var Prophet
     */
    private $prophet;

    function let()
    {
        $this->beConstructedWith(new User(User::ROLE_CUSTOMER), "title", "text");
        $this->prophet = new Prophet();
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Ticket::class);
    }

    public function it_can_be_assigned_to_users()
    {
        $ticket = $this->assignTo(new User(User::ROLE_ADMIN));

        $ticket->shouldBeAnInstanceOf(Ticket::class);
        $ticket->hasState(Ticket::ASSIGNED)->shouldBe(true);
    }

    public function it_can_be_archived()
    {
        $this->archive()->hasState(Ticket::CLOSED)
            ->shouldBe(true);
    }
}
