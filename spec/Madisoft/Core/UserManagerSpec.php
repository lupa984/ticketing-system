<?php

namespace spec\App\Madisoft\Core;

use App\Madisoft\Entity\AbstractUser;
use App\Madisoft\Entity\User;
use App\Madisoft\Core\UserManager;
use App\Madisoft\Exception\UserManagerException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserManagerSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserManager::class);
    }

    public function it_creates_customers()
    {
        $customer = $this->create(AbstractUser::ROLE_CUSTOMER, "fname", "lname", "email@madisoft.it", "password");

        $customer->shouldBeAnInstanceOf(User::class);
        $customer->hasRole(AbstractUser::ROLE_CUSTOMER)->shouldBe(true);
    }

    public function it_creates_customers_fail()
    {
        $this->shouldThrow(UserManagerException::class)
            ->during("create", [AbstractUser::ROLE_CUSTOMER, "fname", "lname", null, "password"]);
    }
}
