<?php

namespace spec\App\Madisoft\Core;

use App\Madisoft\Core\Scheduler;
use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Madisoft\Exception\SchedulerException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SchedulerSpec extends ObjectBehavior
{
    private $admin;
    private $customer;
    private $ticket;

    function let()
    {
        $this->ticket = (new User(User::ROLE_CUSTOMER))->openTicket("title", "text");
        $this->admin = new User(User::ROLE_ADMIN, '', '', 'email_admin1');
        $this->customer = new User(User::ROLE_CUSTOMER, '', '', 'email_customer1');
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Scheduler::class);
    }

    public function it_assigns_tickets()
    {
        $this->assign($this->ticket, $this->admin)->shouldBeAnInstanceOf(Ticket::class);
    }

    public function it_closes_tickets()
    {
        $customer = new User(User::ROLE_CUSTOMER, '', '', 'email1');
        $other_user = new User(User::ROLE_CUSTOMER, '', '', 'email2');
        $admin = new User(User::ROLE_ADMIN);


        $ticket = $customer->openTicket("title", "text");
        $this->assign($ticket, $admin);
        $closed = $this->close($ticket, $admin);
        $closed->shouldBeAnInstanceOf(Ticket::class);
        $closed->hasState(Ticket::CLOSED)->shouldBe(true);


        $ticket = $customer->openTicket("title", "text");
        $this->assign($ticket, $admin);
        $closed = $this->close($ticket, $customer);
        $closed->shouldBeAnInstanceOf(Ticket::class);
        $closed->hasState(Ticket::CLOSED)->shouldBe(true);


        $ticket = $customer->openTicket("title", "text");
        $this->assign($ticket, $admin);
        $this->shouldThrow(SchedulerException::class)->during('close', [$ticket, $other_user]);
    }

    public function it_attaches_messages()
    {
        $other_user = new User(User::ROLE_CUSTOMER, '', '', 'email_customer2');

        $ticket = $this->customer->openTicket("title", "text");
        $this->admin->claim($ticket);

        $this->attachMessage($ticket, $this->admin, "admin message")
            ->shouldHaveType(Message::class);
        $this->attachMessage($ticket, $this->customer, "customer message")
            ->shouldHaveType(Message::class);
        $this->shouldThrow(SchedulerException::class)
            ->during('attachMessage', [$ticket, $other_user, "other user message"]);
    }
}
