<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 10/11/18
 * Time: 00:23
 */

namespace App\Controller;


use App\Form\TicketCreationForm;
use App\Madisoft\Entity\Message;
use App\Service\UserLifecycle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;

/**
 * Class TicketController
 * @package App\Controller
 */
class TicketController extends Controller
{
    /**
     * @param Security $security
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function ticketList(Security $security)
    {
        if(!$security->isGranted("IS_AUTHENTICATED_FULLY")){
            $this->addFlash("danger", "please login");
            return $this->redirectToRoute("app_login");
        }
        if(!$security->isGranted("ROLE_CUSTOMER")){
            throw new HttpException(403, "unauthorized");
        }

        $user = $security->getUser();

        return $this->render("ticket_list.html.twig", [
            "user" => $user
        ]);
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function addTicket(Security $security, Request $request, UserLifecycle $user_service)
    {
        if(!$security->isGranted("ROLE_CUSTOMER")){
            throw new HttpException(403, "unauthorized");
        }

        $form = $this->createForm(TicketCreationForm::class);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                $ticket = $user_service->createTicket($security->getUser(), $form->getData());
                return $this->redirectToRoute('ticket_list');
            }
        }

        return $this->render("add_ticket.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function write(Security $security, Request $request, UserLifecycle $user_service)
    {
        if(!$security->isGranted('ROLE_CUSTOMER')){
            throw new HttpException(403, "unauthorized");
        }

        if(!$request->attributes->has('number')){
            throw new HttpException(400, "missing route parameter");
        }

        if(!$request->request->has('text')){
            throw new HttpException(400, "missing form parameter");
        }

        /**
         * @var Message $message
         */
        $message = $user_service->writeComment(
            $request->attributes->get('number'),
            $request->request->get('text'),
            $security->getUser());


        return $this->redirectToRoute('ticket_list');
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function close(Security $security, Request $request, UserLifecycle $user_service)
    {
        if(!$security->isGranted('ROLE_CUSTOMER')){
            throw new HttpException(403, "unauthorized");
        }

        if(!$request->attributes->has('number')){
            throw new HttpException(400, "missing route parameter");
        }

        $user_service->closeTicket($security->getUser(), $request->attributes->get('number'));

        return $this->redirectToRoute('ticket_list');
    }
}