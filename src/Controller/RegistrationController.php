<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 10:39
 */

namespace App\Controller;


use App\Form\CustomerCreationForm;
use App\Form\DTO\CustomerCreationDTO;
use App\Madisoft\Entity\AbstractUser;
use App\Service\UserLifecycle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegistrationController extends Controller
{
    public function register(UserLifecycle $lifecycle, Request $request)
    {
        $form = $this->createForm(CustomerCreationForm::class, null);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){
                /**
                 * @var CustomerCreationDTO $customer_dto
                 */
                $customer_dto = $form->getData();
                $ret = $lifecycle->registerCustomer(
                    AbstractUser::ROLE_CUSTOMER,
                    $customer_dto->first_name,
                    $customer_dto->last_name,
                    $customer_dto->email,
                    $customer_dto->password
                );
            }
        }

        return $this->render("registration.html.twig", [
            "form" => $form->createView()
        ]);
    }
}