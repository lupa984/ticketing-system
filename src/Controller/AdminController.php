<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 12/11/18
 * Time: 16.04
 */

namespace App\Controller;


use App\Service\UserLifecycle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;

/**
 * Class AdminController
 * @package App\Controller
 */
class AdminController extends Controller
{
    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function claimTicket(Security $security, Request $request, UserLifecycle $user_service)
    {
        if(!$request->attributes->has('number')){
            throw new HttpException(400, "missing parameter");
        }

        $user_service->claimTicket($security->getUser(), $request->attributes->get('number'));

        return $this->redirectToRoute('ticket_list');
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function assignTicket(Security $security, Request $request, UserLifecycle $user_service)
    {
        if(!$request->request->has('ticket_number') || !$request->request->has('admin_selected')){
            throw new HttpException(400, "missing parameter");
        }

        $user = $security->getUser();
        $user_service->assignTicket($user, $request->request->get('admin_selected'), $request->request->get('ticket_number'));

        return $this->redirectToRoute('ticket_list');
    }
}