<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 10:11
 */

namespace App\Form;


use App\Form\DTO\CustomerCreationDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerCreationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('first_name', TextType::class, ["label" => "first name"])
            ->add('last_name', TextType::class, ["label" => "last name"])
            ->add('email', EmailType::class, ["label" => "email"])
            ->add('password', RepeatedType::class, [
                "type" => PasswordType::class,
                "first_options" => ["label" => "password"],
                "second_options" => ["label" => "repeat password"]
            ])
            ->add('submit', SubmitType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomerCreationDTO::class
        ]);
    }

}