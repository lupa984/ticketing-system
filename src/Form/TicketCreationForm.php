<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 10:11
 */

namespace App\Form;


use App\Form\DTO\TicketCreationDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketCreationForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('text')
        ->add('insert', SubmitType::class);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TicketCreationDTO::class
        ]);
    }

}