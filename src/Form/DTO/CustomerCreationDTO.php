<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 00:31
 */

namespace App\Form\DTO;


class CustomerCreationDTO
{
    public $first_name;
    public $last_name;
    public $email;
    public $password;
}