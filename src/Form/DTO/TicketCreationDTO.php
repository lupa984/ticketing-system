<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 00:31
 */

namespace App\Form\DTO;


class TicketCreationDTO
{
    public $title;
    public $text;
}