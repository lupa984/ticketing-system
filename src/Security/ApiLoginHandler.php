<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 16/11/18
 * Time: 12.01
 */

namespace App\Security;


use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ApiLoginHandler
{
    private $request;
    private $em;
    private $user_password_encoder;
    private $lexik_encoder;

    public function __construct(
        RequestStack $stack,
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder,
        JWTEncoderInterface $jwt_ancoder
    )
    {
        $this->request = $stack->getCurrentRequest();
        $this->em = $em;
        $this->user_password_encoder = $encoder;
        $this->lexik_encoder = $jwt_ancoder;
    }

    /**
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    public function authenticate()
    {
        $body = $this->request->getContent();
        $credentials = json_decode($body, true);

        if (!$user = $this->em->getRepository('App:UserModelDTO')
            ->findByEmail($credentials['username'])) {
            throw new HttpException(404, "user {$credentials['username']} not found");
        }

        if (!$this->user_password_encoder->isPasswordValid($user, $credentials['password'])) {
            throw new HttpException(403, "invalid pasword");
        }

        return $this->lexik_encoder->encode($credentials);
    }
}