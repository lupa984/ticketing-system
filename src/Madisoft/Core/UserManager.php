<?php

namespace App\Madisoft\Core;

use App\Madisoft\Entity\User;
use App\Madisoft\Entity\UserManagerInterface;
use App\Madisoft\Exception\UserManagerException;

class UserManager implements UserManagerInterface
{
    public static function create($role, $first_name, $last_name, $email, $plain) : User
    {
        if($role === User::ROLE_CUSTOMER && (!$email || $email === '')){
            throw new UserManagerException("to register a new customer an mail should be provided");
        }

        return new User($role, $first_name, $last_name, $email, $plain);
    }
}
