<?php

namespace App\Madisoft\Core;


use App\Madisoft\Entity\AbstractUser;
use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\TicketSchedulerInterface;
use App\Madisoft\Entity\User;
use App\Madisoft\Exception\SchedulerException;

class Scheduler implements TicketSchedulerInterface
{
    /**
     * @inheritdoc
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public static function create(User $user, $title): Ticket
    {
        $ticket = new Ticket($user, $title);
        return $ticket;
    }

    /**
     * @inheritdoc
     */
    public static function assign(Ticket $ticket, User $to, User $from = null): Ticket
    {
        if (!$to->hasRole(AbstractUser::ROLE_ADMIN)) {
            throw new SchedulerException("cannot assign to non admin user", 403);
        }

        if(!$from && !$ticket->hasState(Ticket::NEW)){
            throw new SchedulerException("only new tickets can be claimed", 400);
        }

        if ($ticket->hasState(Ticket::ASSIGNED) && (!$from || $from && !$ticket->isAssignedTo($from))) {
            throw new SchedulerException("user not authorized to claim assigned ticket", 403);
        }

        if(!$ticket->hasState(Ticket::NEW) && $ticket->getAssignor()->getEmail() === $to->getEmail()){
            throw new SchedulerException("ticket is already assigne to {$to->getEmail()}", 400);
        }

        return $ticket->assignTo($to);
    }

    /**
     * @inheritdoc
     */
    public static function close(?Ticket $ticket, AbstractUser $user): Ticket
    {
        if (!$ticket) {
            throw new SchedulerException("ticket cannot be null", 400);
        }

        if (!$ticket->isAssignedTo($user) && !$ticket->isCreatedBy($user)) {
            throw new SchedulerException("user cannot close ticket {$ticket->getNumber()}. (nor creator or assignor)",
                403);
        }

        if ($ticket->hasState(Ticket::CLOSED)) {
            throw new SchedulerException("ticket already closed", 400);
        }

        $ticket->archive();

        return $ticket;
    }

    /**
     * @inheritdoc
     * @throws SchedulerException
     * @throws \App\Madisoft\Exception\MessageException
     */
    public static function attachMessage(?Ticket $ticket, AbstractUser $user, $text): Message
    {
        if (!$ticket) {
            throw new SchedulerException("ticket cannot be null");
        }

        if (!($ticket->isCreatedBy($user) || $ticket->isAssignedTo($user))) {
            throw new SchedulerException("user is not authorized to attach a message");
        }

        if($ticket->hasState(Ticket::CLOSED)){
            throw new SchedulerException("ticket is closed", 400);
        }

        $message = new Message($user, $ticket, $text);
        return $message;
    }

    /**
     * @inheritdoc
     * @throws SchedulerException
     */
    public static function read(AbstractUser $user, Ticket $ticket): Ticket
    {
        if ($user->hasRole(AbstractUser::ROLE_CUSTOMER) && !$ticket->isCreatedBy($user)) {
            throw new SchedulerException("user not authorized to read ticket", 403);
        }

        return $ticket;
    }
}
