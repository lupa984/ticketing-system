<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 02/11/18
 * Time: 12:11
 */

namespace App\Madisoft\Entity;

/**
 * Interface TicketSchedulerInterface
 * @package App\Madisoft\Entity
 */
interface TicketSchedulerInterface
{
    /**
     * @param AbstractUser $user
     * @param $title
     * @param $text
     * @return Ticket
     */
    public static function create(User $user, $title) : Ticket;
    /**
     * if ticket is in NEW state assigns the ticket to $user and return $ticket instance on success
     * else if provided the actual owner the ticket is assigned to the new user, 403 raised otherwise
     * @param Ticket $ticket
     * @param AbstractUser $to
     * @param AbstractUser $from
     * @return Ticket
     */
    public static function assign(Ticket $ticket, User $to, User $from) : Ticket;

    /**
     * if user is the assignor or the creator the ticket gets closed
     * @param Ticket $ticket
     * @param AbstractUser $user
     * @return Ticket
     */
    public static function close(Ticket $ticket, AbstractUser $user) : Ticket;

    /**
     * @param Ticket $ticket
     * @param AbstractUser $user
     * @param $text
     * @return Ticket
     */
    public static function attachMessage(Ticket $ticket, AbstractUser $user, $text) : Message;

    /**
     * @param AbstractUser $user
     * @param Ticket $ticket
     * @return Ticket
     */
    public static function read(AbstractUser $user, Ticket $ticket) : Ticket;
}