<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 04/11/18
 * Time: 16:44
 */

namespace App\Madisoft\Entity;


interface UserManagerInterface
{
    public static function create($role, $first_name, $last_name, $email, $plain) : User;
}