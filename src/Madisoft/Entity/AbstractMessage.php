<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 03/11/18
 * Time: 17:53
 */

namespace App\Madisoft\Entity;


abstract class AbstractMessage
{
    protected $text;
    protected $author;
    protected $creation_date;

    /**
     * @return AbstractUser
     */
    public function getAuthor(): AbstractUser
    {
        return $this->author;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }

    /**
     * @return string
     */
    public function readContent()
    {
        return $this->text;
    }

    /**
     * @param AbstractUser $author
     * @return bool
     */
    public function hasAuthor(AbstractUser $author)
    {
        return $this->author === $author;
    }
}