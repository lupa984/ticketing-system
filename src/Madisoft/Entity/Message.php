<?php

namespace App\Madisoft\Entity;

use App\Madisoft\Exception\MessageException;

class Message extends AbstractMessage
{
    private $ticket;

    /**
     * @return Ticket
     */
    public function getTicket(): Ticket
    {
        return $this->ticket;
    }

    /**
     * Message constructor.
     * @param AbstractUser $author
     * @param Ticket $ticket
     * @param $text
     * @param null $creation_date
     * @throws MessageException
     */
    public function __construct(AbstractUser $author, Ticket $ticket, $text, $creation_date = null)
    {
        if (!$author) {
            throw new MessageException("author should be provided", 422);
        }

        if (!$ticket) {
            throw new MessageException("ticket should be provided", 422);
        }

        $text = trim($text);
        if (!$text || $text === '') {
            throw new MessageException("null or empty message", 422);
        }

        $this->author = $author;
        $this->ticket = $ticket;
        $this->text = $text;

        if (!$this->creation_date = $creation_date) {
            $this->creation_date = new \DateTime();
        }
    }
}
