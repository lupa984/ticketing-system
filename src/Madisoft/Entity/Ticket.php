<?php

namespace App\Madisoft\Entity;


use App\Madisoft\Exception\TicketException;

/**
 * Class Ticket
 * @package App\Madisoft\Entity
 */
class Ticket extends AbstractTicket
{
    private $title;
    private $creator;
    private $assignor;
    private $last_updated;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return AbstractUser
     */
    public function getCreator(): AbstractUser
    {
        return $this->creator;
    }

    /**
     * @return mixed
     */
    public function getAssignor()
    {
        return $this->assignor;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdated(): ?\DateTime
    {
        return $this->last_updated;
    }

    /**
     * Ticket constructor.
     * @param $creator
     * @param $title
     * @param $text
     * @throws TicketException
     */
    public function __construct($creator, $title, $state = null, $number = null, $creation_date = null, $last_updated = null, $assignor = null)
    {
        if (!$creator || !$creator instanceof AbstractUser) {
            throw new TicketException("null or invalid creator provided", 422);
        }

        if (!$title || $title === '') {
            throw new TicketException("ticket title is missing", 422);
        }

        if (!$this->number = $number) {
            $this->number = uniqid();
        }

        if (!$this->creation_date = $creation_date) {
            $this->creation_date = new \DateTime();
        }

        if ($this->last_updated = $last_updated) {
            $this->last_updated = new \DateTime();
        }

        if (!$this->state = $state) {
            $this->state = Ticket::NEW;
        }

        $this->creator = $creator;
        $this->assignor = $assignor;
        $this->title = trim($title);
    }

    /**
     * @param AbstractUser $user
     * @return bool
     */
    public function isCreatedBy(User $user)
    {
        return $this->creator->getEmail() === $user->getEmail();
    }

    /**
     * @param AbstractUser $user
     * @return bool
     */
    public function isAssignedTo(User $user)
    {
        return $this->assignor && $this->assignor->getEmail() === $user->getEmail();
    }

    /**
     * @param AbstractUser $user
     * @return $this
     */
    public function assignTo(AbstractUser $user)
    {
        $this->assignor = $user;
        $this->state = Ticket::ASSIGNED;
        $this->last_updated = new \DateTime();
        return $this;
    }

    /**
     * @return $this
     */
    public function archive()
    {
        $this->state = Ticket::CLOSED;
        $this->last_updated = new \DateTime();
        return $this;
    }
}
