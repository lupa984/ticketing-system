<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 02/11/18
 * Time: 10:44
 */

namespace App\Madisoft\Entity;

/**
 * Class AbstractTicket
 * @package App\Madisoft\Entity
 */
abstract class AbstractTicket
{
    const NEW = 0;
    const ASSIGNED = 10;
    const CLOSED = 20;

    protected $number;
    protected $state;
    protected $creation_date;

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creation_date;
    }

    /**
     * @param $state
     * @return bool
     */
    public function hasState($state) : bool
    {
        return $this->state === $state;
    }
}