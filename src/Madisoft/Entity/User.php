<?php

namespace App\Madisoft\Entity;

use App\Madisoft\Core\Scheduler;
use App\Madisoft\Exception\MadisoftException;
use App\Madisoft\Exception\TicketException;

/**
 * Class User
 * @package App\Madisoft\Entity
 */
class User extends AbstractUser
{
    private $first_name;
    private $last_name;
    private $email;
    private $plain;

    public function __construct($role, $first_name = '', $last_name = '', $email = '', $plain = null)
    {
        parent::__construct($role);
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->email = $email;
        $this->plain = $plain;
    }

    /**
     * @return null
     */
    public function getPlain()
    {
        return $this->plain;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->last_name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param $title
     * @param $text
     * @return Ticket
     * @throws \App\Madisoft\Exception\TicketException
     * @throws \App\Madisoft\Exception\MessageException
     */
    public function openTicket($title)
    {
        return Scheduler::create($this, $title);
    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     * @throws TicketException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function claim(Ticket $ticket)
    {
        $ticket = Scheduler::assign($ticket, $this);

        if(!$ticket->assignTo($this)){
            throw new TicketException("unable to claim the ticket", 500);
        }

        return $ticket;
    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     * @throws TicketException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function close(?Ticket $ticket)
    {
        $closed = Scheduler::close($ticket, $this);

        if(!$closed || !$closed->hasState(Ticket::CLOSED)){
            throw new TicketException("unable to close the ticket", 500);
        }

        return $closed;
    }

    public function assign(Ticket $ticket, AbstractUser $user)
    {
        $assigned = Scheduler::assign($ticket, $user, $this);

        if(!$assigned->hasState(Ticket::ASSIGNED) && $assigned->isAssignedTo($user)){
            throw new TicketException("unable to assign ticket", 500);
        }

        return $assigned;
    }

    /**
     * @param Ticket $ticket
     * @param $text
     * @return bool|Message
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function writeMessage(?Ticket $ticket, $text)
    {
//        try {
            return Scheduler::attachMessage($ticket, $this, $text);
//        } catch (MadisoftException $e) {
//            dump($e->getMessage());
//            return false;
//        }
    }

    /**
     * @param Ticket $ticket
     * @return Ticket
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function read(Ticket $ticket)
    {
        return Scheduler::read($this, $ticket);
    }
}
