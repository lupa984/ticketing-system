<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 02/11/18
 * Time: 10:59
 */

namespace App\Madisoft\Entity;

/**
 * Class AbstractUser
 * @package App\Madisoft\Entity
 */
abstract class AbstractUser
{
    const ROLE_ADMIN = "ROLE_ADMIN";
    const ROLE_CUSTOMER = "ROLE_CUSTOMER";
    protected $role;

    /**
     * AbstractUser constructor.
     * @param $role
     */
    public function __construct($role)
    {
        $this->role = $role;
    }

    /**
     * @param $role
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->role === $role;
    }
}