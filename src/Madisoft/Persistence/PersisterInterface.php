<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 05/11/18
 * Time: 21:48
 */

namespace App\Madisoft\Persistence;


use App\Madisoft\Exception\PersisterException;

interface PersisterInterface
{
    /**
     * @param $entity
     * @return object
     * @throws PersisterException
     */
    public function save($entity);
}