<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 07/11/18
 * Time: 21:33
 */

namespace App\Madisoft\Persistence;


use App\Madisoft\Entity\User;

interface UserPersisterInterface
{
    public function save(User $user) : ?User;

    public function retrieve($email) : ?User;
}