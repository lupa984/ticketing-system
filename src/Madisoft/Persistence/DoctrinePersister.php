<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 07/11/18
 * Time: 21:46
 */

namespace App\Madisoft\Persistence;


use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DoctrinePersister
{
    protected $em;
    protected $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }
}