<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 07/11/18
 * Time: 21:33
 */

namespace App\Madisoft\Persistence;


use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Model\DTO\UserModelDTO;

interface TicketPersisterInterface
{
    public function save(Ticket $ticket) : ?Ticket;

    public function addMessage(Message $message) : ?Message;

    public function retrieve($number) : ?Ticket;

    public function findForUser($email) : array;

    public function findTicketMessages($ticket_number) : array;

    public function findClosed($email) : array;

}