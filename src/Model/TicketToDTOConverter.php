<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 06/11/18
 * Time: 19:42
 */

namespace App\Model;


use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Model\DTO\TicketModelDTO;
use App\Model\DTO\UserModelDTO;

class TicketToDTOConverter
{
    public static function convert(?Ticket $entity, UserModelDTO $creator, ?UserModelDTO $assignor)
    {
        if (!$entity) {
            return null;
        }

        $dto = new TicketModelDTO();
        $dto->number = $entity->getNumber();
        $dto->state = $entity->getState();
        $dto->creation_date = $entity->getCreationDate();
        $dto->creator = $creator;
        $dto->assignor = $assignor;
        $dto->title = $entity->getTitle();
        return $dto;
    }

    /**
     * @param TicketModelDTO $dto
     * @param User|null $user
     * @return Ticket
     * @throws \App\Madisoft\Exception\TicketException
     */
    public static function revert(TicketModelDTO $dto, User $user = null)
    {
        if (!$user) {
            $user = UserToDTOConverter::revert($dto->creator);
        }

        if($assignor = $dto->assignor){
            $assignor = UserToDTOConverter::revert($assignor);
        }

        $ticket = new Ticket(
            $user,
            $dto->title,
            $dto->state,
            $dto->number,
            $dto->creation_date,
            $dto->last_updated,
            $assignor
        );

        return $ticket;
    }
}