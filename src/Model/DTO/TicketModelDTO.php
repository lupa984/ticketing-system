<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 10/11/18
 * Time: 23:29
 */

namespace App\Model\DTO;


class TicketModelDTO
{
    public $id;
    public $number;
    public $state;
    public $title;
    public $text;
    public $creator;
    public $assignor;
    public $last_updated;
    public $creation_date;
}