<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 10/11/18
 * Time: 23:29
 */

namespace App\Model\DTO;


class MessageModelDTO
{
    public $id;
    public $text;
    public $author;
    public $ticket;
    public $creation_date;

    public function __construct($creation_date = null)
    {
        if(!$this->creation_date = $creation_date){
            $this->creation_date = new \DateTime();
        }
    }
}