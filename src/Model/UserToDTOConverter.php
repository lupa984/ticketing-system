<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 06/11/18
 * Time: 19:42
 */

namespace App\Model;


use App\Madisoft\Entity\User;
use App\Model\DTO\UserModelDTO;

class UserToDTOConverter
{
    public static function convert(?User $entity)
    {
        if (!$entity) {
            return null;
        }

        $dto = new UserModelDTO();
        $dto->role = $entity->getRole();
        $dto->first_name = $entity->getFirstName();
        $dto->last_name = $entity->getLastName();
        $dto->email = $entity->getEmail();
        $dto->plain = $entity->getPlain();
        return $dto;
    }

    public static function revert(?UserModelDTO $dto)
    {
        if (!$dto) {
            return null;
        }

        return new User($dto->role, $dto->first_name, $dto->last_name, $dto->email);
    }
}