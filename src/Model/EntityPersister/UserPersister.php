<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 07/11/18
 * Time: 21:36
 */

namespace App\Model\EntityPersister;


use App\Madisoft\Entity\User;
use App\Madisoft\Persistence\DoctrinePersister;
use App\Madisoft\Persistence\UserPersisterInterface;
use App\Model\UserToDTOConverter;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Log\Logger;

/**
 * Class UserPersister
 * @package App\Model\EntityPersister
 */
class UserPersister extends DoctrinePersister implements UserPersisterInterface
{
    /**
     * @param User $user
     * @return User|null
     */
    public function save(User $user): ?User
    {
        $user_dto = UserToDTOConverter::convert($user);

        try{
            $this->em->getRepository('App:UserModelDTO')->save($user_dto);
            $this->em->flush();
            return $user;
        }
        catch (\Exception $e){

        }

        return null;
    }

    public function retrieve($email) : ?User
    {
        return UserToDTOConverter::revert($this->em->getRepository('App:UserModelDTO')->findByEmail($email));
    }
}