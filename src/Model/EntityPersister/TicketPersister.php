<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 07/11/18
 * Time: 21:36
 */

namespace App\Model\EntityPersister;


use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Madisoft\Persistence\DoctrinePersister;
use App\Madisoft\Persistence\TicketPersisterInterface;
use App\Model\DTO\MessageModelDTO;
use App\Model\DTO\UserModelDTO;
use App\Model\MessageToDTOConverter;
use App\Model\TicketToDTOConverter;
use App\Model\UserToDTOConverter;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class UserPersister
 * @package App\Model\EntityPersister
 */
class TicketPersister extends DoctrinePersister implements TicketPersisterInterface
{
    /**
     * @param Ticket $ticket
     * @return User|null
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function save(Ticket $ticket): ?Ticket
    {
        $creator_dto = $this->em->getRepository('App:UserModelDTO')
            ->findByEmail($ticket->getCreator()->getEmail());

        if (!$ticket_dto = $this->em->getRepository('App:TicketModelDTO')
            ->findByNumber($ticket->getNumber())) {
            $ticket_dto = TicketToDTOConverter::convert($ticket, $creator_dto, null);
        }

        $assignor = $ticket->getAssignor();
        if($assignor && !$assignor = $this->em->getRepository('App:UserModelDTO')->findByEmail($ticket->getAssignor()->getEmail())){
            $assignor = UserToDTOConverter::convert($ticket->getAssignor());
        }
        $ticket_dto->assignor = $assignor;
        $ticket_dto->state = $ticket->getState();
        $ticket_dto->last_updated = $ticket->getLastUpdated();

        try {
            $this->em->getConnection()->beginTransaction();
            $this->em->getRepository('App:TicketModelDTO')->save($ticket_dto);
            $this->em->flush();
            $this->em->getConnection()->commit();
            return $ticket;
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
        }


        return null;
    }

    /**
     * @param $ticket_number
     * @param $text
     * @param User $author
     * @return Message|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addMessage(Message $message): ?Message
    {
        $ticket_dto = $this->em->getRepository('App:TicketModelDTO')
            ->findByNumber($message->getTicket()->getNumber());

        $author_dto = $this->em->getRepository('App:UserModelDTO')
            ->findByEmail($message->getAuthor()->getEmail());


        $message_dto = MessageToDTOConverter::convert($message, $author_dto, $ticket_dto);

        $author = UserToDTOConverter::revert($author_dto);

        try {
            $this->em->getRepository('App:MessageModelDTO')->save($message_dto);
            $this->em->flush();
            return MessageToDTOConverter::revert(
                $message_dto,
                $author,
                TicketToDTOConverter::revert($ticket_dto, $author));
        } catch (\Exception $e) {

        }

        return null;
    }

    /**
     * @param $number
     * @return Ticket|null
     * @throws \App\Madisoft\Exception\TicketException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function retrieve($number) : ?Ticket
    {
        if(!$dto = $this->em->getRepository('App:TicketModelDTO')
            ->findByNumber($number)){
            return null;
        }

        $creator = UserToDTOConverter::revert($dto->creator);
        $assignor = UserToDTOConverter::revert($dto->assignor);
        return TicketToDTOConverter::revert($dto, $creator, $assignor);
    }

    /**
     * @return mixed
     */
    public function unassigned()
    {
        return $this->em->getRepository('App:TicketModelDTO')
            ->findUnassigned();
    }

    /**
     * @param string $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findForUser($email) : array
    {
        if(!$user = $this->em->getRepository('App:UserModelDTO')->findByEmail($email)){
            throw new HttpException(400, "email not registered");
        }

        if($user->role === User::ROLE_ADMIN){
            return $this->em->getRepository('App:TicketModelDTO')
                ->findAssignedTo($user);
        }

        if($user->role === User::ROLE_CUSTOMER){
            return $this->em->getRepository('App:TicketModelDTO')
                ->findCreatedBy($user);
        }

        throw new HttpException(403, "unauthorized");
    }

    /**
     * @param $ticket_number
     * @return array
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findTicketMessages($ticket_number): array
    {
        $ticket = $this->em->getRepository('App:TicketModelDTO')->findByNumber($ticket_number);
        return $this->em->getRepository('App:MessageModelDTO')->findOfTicket($ticket);
    }

    /**
     * @param $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findClosed($email) : array
    {
        $user_dto = $this->em->getRepository('App:UserModelDTO')->findByEmail($email);
        return $this->em->getRepository('App:TicketModelDTO')->findClosedForUser($user_dto);
    }
}