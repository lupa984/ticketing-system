<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 06/11/18
 * Time: 19:42
 */

namespace App\Model;


use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Entity\User;
use App\Model\DTO\MessageModelDTO;
use App\Model\DTO\TicketModelDTO;
use App\Model\DTO\UserModelDTO;

class MessageToDTOConverter
{
    public static function convert(?Message $entity, UserModelDTO $user_dto, TicketModelDTO $ticket_dto)
    {
        if (!$entity) {
            return null;
        }

        $dto = new MessageModelDTO();
        $dto->text = $entity->getText();
        $dto->ticket = $ticket_dto;
        $dto->author = $user_dto;
        return $dto;
    }

    public static function revert(MessageModelDTO $dto, User $author, Ticket $ticket)
    {
        return new Message($author, $ticket, $dto->text, $dto->creation_date);
    }
}