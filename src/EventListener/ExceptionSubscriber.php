<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 18/11/18
 * Time: 23:58
 */

namespace App\EventListener;


use App\Madisoft\Exception\MadisoftException;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionSubscriber
 * @package App\EventListener
 */
class ExceptionSubscriber implements EventSubscriberInterface
{
    private $session;
    private $templating;

    public function __construct(
        EngineInterface $twig,
        SessionInterface $session)
    {
        $this->session = $session;
        $this->templating = $twig;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (strpos($event->getRequest()->getRequestUri(), "/api") === 0) {
            return;
        }

        $ex = $event->getException();

        if ($ex instanceof MadisoftException || $ex instanceof HttpException) {
            $event->setResponse(
                $this->templating->renderResponse("error_page.html.twig")
            );
        }
        $this->session->getFlashBag()->add("danger", $ex->getMessage());
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }
}