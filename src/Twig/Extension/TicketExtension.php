<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 12/11/18
 * Time: 14.43
 */

namespace App\Twig\Extension;


use App\Madisoft\Entity\User;
use App\Madisoft\Persistence\TicketPersisterInterface;
use App\Model\DTO\TicketModelDTO;
use App\Model\DTO\UserModelDTO;
use App\Model\EntityPersister\TicketPersister;
use App\Model\UserToDTOConverter;
use App\Repository\TicketRepository;
use App\Service\TicketLifecycle;
use App\Service\UserLifecycle;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class TicketExtension
 * @package App\Twig\Extension
 */
class TicketExtension extends AbstractExtension
{
    private $ticket_persister;
    private $user_service;
    private $ticket_service;

    /**
     * TicketExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(TicketPersister $ticketPersister, UserLifecycle $user_service, TicketLifecycle $ticket_service)
    {
        $this->ticket_persister = $ticketPersister;
        $this->user_service = $user_service;
        $this->ticket_service = $ticket_service;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
          new TwigFunction('unassigned', [$this, 'getUnassigned']),
          new TwigFunction('user_tickets', [$this, 'getUserTickets']),
          new TwigFunction('ticket_messages', [$this, 'getTicketMessages']),
          new TwigFunction('closed', [$this, 'getClosedTickets'])
        ];
    }

    /**
     * @param User $user
     * @return array|mixed
     */
    public function getUnassigned(UserModelDTO $user)
    {
        $unassigned = [];
        if($user->role === User::ROLE_ADMIN){
            $unassigned = $this->ticket_persister->unassigned();
        }

        return $unassigned;
    }

    public function getUserTickets(UserModelDTO $user)
    {
        return $this->user_service->getTickets($user);
//        return $this->ticket_persister->findForUser($user->email);
    }

    public function getTicketMessages(TicketModelDTO $ticket)
    {
        return $this->ticket_service->getTicketMessages($ticket);
//        return $this->ticket_persister->findTicketMessages($ticket->number);
    }

    public function getClosedTickets(UserModelDTO $user)
    {
        return $this->user_service->getClosed($user);
//        return $this->ticket_persister->findClosed($user->email);
    }
}