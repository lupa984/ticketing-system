<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 12/11/18
 * Time: 14.52
 */

namespace App\Twig\Extension;


use App\Madisoft\Entity\User;
use App\Model\DTO\UserModelDTO;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class UserExtension
 * @package App\Twig\Extension
 */
class UserExtension extends AbstractExtension
{
    private $user_repository;
    private $admins;

    /**
     * UserExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->user_repository = $em->getRepository('App:UserModelDTO');
        $this->admins = [];
    }

    /**
     * @return array|TwigFunction|\Twig_Function[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('admins', [$this, 'getAdmins'])
        ];
    }

    /**
     * @param UserModelDTO $user
     * @return array|mixed
     */
    public function getAdmins(UserModelDTO $user)
    {
        if ($user->role === User::ROLE_ADMIN) {
            if (!count($this->admins)) {
                $this->admins = $this->user_repository->getAdmins($user);
            }
        }

        return $this->admins;
    }
}