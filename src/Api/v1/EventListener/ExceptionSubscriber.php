<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 17/11/18
 * Time: 17:16
 */

namespace App\Api\v1\EventListener;


use App\Madisoft\Exception\MadisoftException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ExceptionSubscriber
 * @package App\Api\v1\EventListener
 */
class ExceptionSubscriber implements EventSubscriberInterface
{

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if (strpos($event->getRequest()->getRequestUri(), "/api") !== 0) {
            return;
        }

        $ex = $event->getException();

        if ($ex instanceof MadisoftException) {
            $event->setResponse(new JsonResponse([
                    "status" => "danger",
                    "message" => "logical exception",
                    "detail" => $ex->getMessage()
                ])
            );
        } elseif ($ex instanceof HttpException) {
            $event->setResponse(new JsonResponse([
                    "status" => "danger",
                    "message" => "application exception",
                    "detail" => $ex->getMessage()
                ])
            );
        } else {
            return;
        }
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }
}