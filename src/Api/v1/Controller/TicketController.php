<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 15/11/18
 * Time: 11.16
 */

namespace App\Api\v1\Controller;


use App\Form\DTO\TicketCreationDTO;
use App\Service\UserLifecycle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;

class TicketController extends BaseController
{
    /**
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function ticketList(UserLifecycle $user_service)
    {
        $user = $this->getUser();
        $tickets = $user_service->getTickets($user);

        return $this->createApiResponse([
            'tickets' => $tickets
        ]);
    }

    /**
     * requires request body to have title field. Text field is optional
     * @param Request $request
     * @param Security $security
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function addTicket(Request $request, Security $security, UserLifecycle $user_service)
    {
        if (!$security->isGranted("ROLE_CUSTOMER")) {
            throw new HttpException(403, "unauthorized");
        }

        if ($security->isGranted("ROLE_ADMIN")) {
            throw new HttpException(400, "admins cannot add tickets");
        }

        $data = json_decode($request->getContent(), true);
        $ticket_dto = new TicketCreationDTO();
        $ticket_dto->title = $data['title'];
        $ticket_dto->text = $data['text'];

        return $this->createApiResponse([
            "status" => "success",
            "ticket" => $user_service->createTicket($security->getUser(), $ticket_dto)
        ]);
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function write(Security $security, Request $request, UserLifecycle $user_service)
    {
        $data = json_decode($request->getContent(), true);
        return $this->createApiResponse(
            [
                "status" => "success",
                "message" => $user_service->writeComment(
                    $data['ticket'],
                    $data['text'],
                    $security->getUser()
                )
            ]);
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function close(Security $security, Request $request, UserLifecycle $user_service)
    {
        $data = json_decode($request->getContent(), true);

        if ($user_service->closeTicket($security->getUser(), $data['ticket'])) {
            return $this->createApiResponse([
                "status" => "success",
                "message" => "ticket {$data['ticket']} closed successfully"
            ]);
        }
        return $this->createApiResponse([
            "status" => "danger",
            "message" => "could not close ticket {$data['ticket']}"
        ]);

    }
}