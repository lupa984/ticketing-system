<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 17/11/18
 * Time: 00:17
 */

namespace App\Api\v1\Controller;


use App\Service\UserLifecycle;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Security;

class AdminController extends BaseController
{
    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function claimTicket(Security $security, Request $request, UserLifecycle $user_service)
    {
        if (!$security->isGranted('ROLE_ADMIN')) {
            throw new HttpException(403, "unauthorized");
        }

        $data = json_decode($request->getContent(), true);

        if (!isset($data['ticket'])) {
            throw new HttpException(422, "missing parameter");
        }

        try {
            if ($user_service->claimTicket($security->getUser(), $data['ticket'])) {
                return $this->createApiResponse([
                    'status' => 'success',
                    'message' => "ticket {$data['ticket']} claimed successfully"
                ]);
            }
            return $this->createApiResponse([
                'status' => 'danger',
                'message' => "could not claim ticket {$data['ticket']}"
            ]);
        } catch (\Exception $e) {
            return $this->createApiResponse([
                'status' => 'danger',
                'message' => "could not claim ticket {$data['ticket']}",
                'details' => $e->getMessage()
            ]);
        }
    }

    /**
     * @param Security $security
     * @param Request $request
     * @param UserLifecycle $user_service
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function assignTicket(Security $security, Request $request, UserLifecycle $user_service)
    {
        if (!$security->isGranted('ROLE_ADMIN')) {
            throw new HttpException(403, "unauthorized");
        }

        $data = json_decode($request->getContent(), true);

        if (!isset($data['ticket']) || !isset($data['admin'])) {
            throw new HttpException(422, "missing parameter");
        }

        try {
            if ($user_service->assignTicket(
                $security->getUser(),
                $data['admin'],
                $data['ticket']
            )) {
                return $this->createApiResponse(
                    [
                        'status' => 'success',
                        'message' => "ticket {$data['ticket']} assigned successfully to {$data['admin']}"
                    ]
                );
            }

            return $this->createApiResponse(
                [
                    'status' => 'danger',
                    'message' => "ticket {$data['ticket']} cannot be assigned to {$data['admin']}"
                ]
            );
        } catch (\Exception $e) {
            return $this->createApiResponse(
                [
                    'status' => 'danger',
                    'message' => "ticket {$data['ticket']} cannot be assigned to {$data['admin']}",
                    'details' => $e->getMessage()
                ]
            );
        }
    }
}