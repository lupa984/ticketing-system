<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 15/11/18
 * Time: 10.04
 */

namespace App\Api\v1\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class BaseController extends Controller
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param string $format
     * @return string
     */
    protected function serialize($data, $format = 'json')
    {
        return $this->serializer->serialize($data, $format);
    }

    /**
     * @param $data
     * @param int $statusCode
     * @return Response
     */
    protected function createApiResponse($data, $statusCode = 200)
    {
        $json = $this->serialize($data);

        return new Response($json, $statusCode, array(
            'Content-Type' => 'application/json'
        ));
    }
}