<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 15/11/18
 * Time: 11.18
 */

namespace App\Api\v1\Controller;


use App\Security\ApiLoginHandler;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends BaseController
{
    public function login(ApiLoginHandler $handler)
    {
        try {
            $response = $this->createApiResponse([
                'status' => 'success',
                'message' => 'login successfull'
            ]);
            $response->headers->set('Authorization', "Bearer {$handler->authenticate()}");
        } catch (\Exception $e) {
            throw $e;
//            $response = $this->createApiResponse([
//                'status' => 'danger',
//                'message' => 'login failure',
//                'details' => $e->getMessage(),
//                'trace' => $e->getTraceAsString()
//            ], 400);
        }
        return $response;
    }
}