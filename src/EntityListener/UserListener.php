<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 09/11/18
 * Time: 14.26
 */

namespace App\EntityListener;


use App\Model\DTO\UserModelDTO;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserListener
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function preUpdate(UserModelDTO $user, PreUpdateEventArgs $args)
    {
        if($user->plain){
            $encoded = $this->encoder->encodePassword($user, $user->plain);
            $user->password = $encoded;
        }
    }

    public function prePersist(UserModelDTO $user, LifecycleEventArgs $args)
    {
        if($user->plain){
            $encoded = $this->encoder->encodePassword($user, $user->plain);
            $user->password = $encoded;
        }
    }
}