<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 19/11/18
 * Time: 11.34
 */

namespace App\Notification;


interface NotificationInterface
{
    public function notify(string $content, array $to);
}