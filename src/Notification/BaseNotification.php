<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 19/11/18
 * Time: 11.43
 */

namespace App\Notification;


/**
 * Class BaseNotification
 * @package App\Notification
 */
abstract class BaseNotification
{
    const PUSH_NOTIFICATION = 0;
    const SMS_NOTIFICATION = 1;
    const EMAIL_NOTIFICATION = 2;
}