<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 19/11/18
 * Time: 12.02
 */

namespace App\Service;


use App\Model\DTO\MessageModelDTO;
use App\Model\DTO\TicketModelDTO;
use App\Model\DTO\UserModelDTO;
use App\Notification\EmailNotifier;
use App\Notification\PushNotifier;
use App\Notification\SMSNotifier;

/**
 * Class Notifier
 * @package App\Service
 */
class Notifier
{
    private $notifier;

    /**
     * @param SMSNotifier $notifier
     */
    public function getSmsNotifier(SMSNotifier $notifier)
    {
        $this->notifier = $notifier;
    }

    /**
     * @param PushNotifier $notifier
     */
    public function getPushNotifier(PushNotifier $notifier)
    {
        $this->notifier = $notifier;
    }

    /**
     * @param EmailNotifier $notifier
     */
    public function getEmailNotifier(EmailNotifier $notifier)
    {
        $this->notifier = $notifier;
    }

    public function notifyTicketCreation(TicketModelDTO $ticket)
    {

    }

    public function notifyTicketClaim(TicketModelDTO $ticket)
    {

    }

    public function notifyTicketAssignment(TicketModelDTO $ticket)
    {

    }

    public function notifyTicketClosed(TicketModelDTO $ticket, UserModelDTO $user)
    {

    }

    public function notifyNewMessage(MessageModelDTO $message)
    {

    }
}