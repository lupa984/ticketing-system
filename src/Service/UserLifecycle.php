<?php

namespace App\Service;

use App\Form\DTO\TicketCreationDTO;
use App\Madisoft\Core\UserManager;
use App\Madisoft\Entity\Message;
use App\Madisoft\Entity\Ticket;
use App\Madisoft\Exception\UserManagerException;
use App\Madisoft\Persistence\TicketPersisterInterface;
use App\Madisoft\Persistence\UserPersisterInterface;
use App\Model\DTO\UserModelDTO;
use App\Model\UserToDTOConverter;

/**
 * Class UserLifecycle
 * @package App\Service
 */
class UserLifecycle
{
    /**
     * @var UserPersisterInterface
     */
    private $user_persister;

    /**
     * @var TicketPersisterInterface
     */
    private $ticket_persister;

    /**
     * @param UserPersisterInterface $user_persister
     */
    public function getUserPersister(UserPersisterInterface $user_persister)
    {
        $this->user_persister = $user_persister;
    }

    /**
     * @param TicketPersisterInterface $ticket_persister
     */
    public function getTicketPersister(TicketPersisterInterface $ticket_persister)
    {
        $this->ticket_persister = $ticket_persister;
    }

    /**
     * @param $role
     * @param $first_name
     * @param $last_name
     * @param $email
     * @return \App\Madisoft\Entity\User
     * @throws UserManagerException
     */
    public function registerCustomer($role, $first_name, $last_name, $email, $plain)
    {
        $user = UserManager::create($role, $first_name, $last_name, $email, $plain);
        return $this->user_persister->save($user);
    }

    /**
     * @param UserModelDTO $user
     * @return array
     */
    public function getTickets(UserModelDTO $user)
    {
        return $this->ticket_persister->findForUser($user->email);
    }

    /**
     * @param UserModelDTO $user
     * @return array
     */
    public function getClosed(UserModelDTO $user)
    {
        return $this->ticket_persister->findClosed($user->email);
    }

    /**
     * @param UserModelDTO $userDTO
     * @param TicketCreationDTO $ticketDTO
     * @return Ticket|null
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function createTicket(UserModelDTO $userDTO, TicketCreationDTO $ticketDTO, Notifier $notifier)
    {
        $user = UserToDTOConverter::revert($userDTO);
        $ticket = $user->openTicket($ticketDTO->title);
        $ticket = $this->ticket_persister->save($ticket);

        if (false !== $message = $user->writeMessage($ticket, $ticketDTO->text)) {
            $this->ticket_persister->addMessage($message);
            $notifier->notifyTicketCreation($ticketDTO);
            return $ticket;
        }

        return null;
    }

    /**
     * @param $ticket_number
     * @param $text
     * @param UserModelDTO $author
     * @return Message|bool|null
     * @throws \App\Madisoft\Exception\MessageException
     * @throws \App\Madisoft\Exception\SchedulerException
     */
    public function writeComment($ticket_number, $text, UserModelDTO $author, Notifier $notifier)
    {
        $user = UserToDTOConverter::revert($author);
        $ticket = $this->ticket_persister->retrieve($ticket_number);
        $message = $user->writeMessage($ticket, $text);
        $message = $this->ticket_persister->addMessage($message);

        if ($message) {
            $notifier->notifyNewMessage($message);
        }

        return $message;
    }

    /**
     * @param UserModelDTO $user_dto
     * @param $ticket_number
     * @return bool
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function claimTicket(UserModelDTO $user_dto, $ticket_number, Notifier $notifier)
    {
        $user = UserToDTOConverter::revert($user_dto);
        $ticket = $this->ticket_persister->retrieve($ticket_number);
        $ticket = $user->claim($ticket);
        $ticket = $this->ticket_persister->save($ticket);

        $notifier->notifyTicketClaim($ticket);

        return $ticket !== null;
    }

    /**
     * @param UserModelDTO $user_dto_from
     * @param $email_to
     * @param $ticket_number
     * @return bool
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function assignTicket(UserModelDTO $user_dto_from, $email_to, $ticket_number, Notifier $notifier)
    {
        $from_user = UserToDTOConverter::revert($user_dto_from);
        $to_user = $this->user_persister->retrieve($email_to);
        $ticket = $this->ticket_persister->retrieve($ticket_number);
        $ticket = $from_user->assign($ticket, $to_user);

        $notifier->notifyTicketAssignment($ticket);

        return $this->ticket_persister->save($ticket) !== null;
    }

    /**
     * @param UserModelDTO $user
     * @param $ticket_number
     * @return bool
     * @throws \App\Madisoft\Exception\SchedulerException
     * @throws \App\Madisoft\Exception\TicketException
     */
    public function closeTicket(UserModelDTO $user_dto, $ticket_number, Notifier $notifier)
    {
        $user = UserToDTOConverter::revert($user_dto);
        $ticket = $this->ticket_persister->retrieve($ticket_number);
        $ticket = $user->close($ticket);

        $notifier->notifyTicketClosed($ticket, $user_dto);

        return $this->ticket_persister->save($ticket) !== null;
    }
}
