<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 16/11/18
 * Time: 15.05
 */

namespace App\Service;


use App\Madisoft\Persistence\TicketPersisterInterface;
use App\Model\DTO\TicketModelDTO;

class TicketLifecycle
{
    private $ticket_persister;

    public function __construct(TicketPersisterInterface $ticket_persister)
    {
        $this->ticket_persister = $ticket_persister;
    }

    public function getTicketMessages(TicketModelDTO $ticket)
    {
        return $this->ticket_persister->findTicketMessages($ticket->number);
    }
}