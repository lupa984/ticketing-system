<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 09/11/18
 * Time: 00:17
 */

namespace App\Repository;


use App\Model\DTO\MessageModelDTO;
use App\Model\DTO\TicketModelDTO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * Class UserRepository
 * @package App\Repository
 */
class MessageRepository extends EntityRepository
{

    /**
     * @param MessageModelDTO $dto
     * @return bool
     * @throws ORMException
     */
    public function save(MessageModelDTO $dto)
    {
        $this->getEntityManager()->persist($dto);
        return true;
    }

    /**
     * @param TicketModelDTO $ticket
     * @return mixed
     */
    public function findOfTicket(TicketModelDTO $ticket)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.ticket = :ticket')->setParameter('ticket', $ticket);
        return $qb->getQuery()->execute();
    }
}