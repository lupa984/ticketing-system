<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 09/11/18
 * Time: 00:17
 */

namespace App\Repository;


use App\Madisoft\Entity\Ticket;
use App\Model\DTO\TicketModelDTO;
use App\Model\DTO\UserModelDTO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * Class UserRepository
 * @package App\Repository
 */
class TicketRepository extends EntityRepository
{

    /**
     * @param TicketModelDTO $dto
     * @return bool
     * @throws ORMException
     */
    public function save(TicketModelDTO $dto)
    {
        $this->getEntityManager()->persist($dto);
        return true;
    }

    /**
     * @param $number
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByNumber($number)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.number = :number')->setParameter('number', $number);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function findUnassigned()
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.assignor is null');
        $qb->andWhere('t.state = :state')->setParameter('state', Ticket::NEW);
        return $qb->getQuery()->execute();
    }

    /**
     * @param UserModelDTO $user
     * @return mixed
     */
    public function findCreatedBy(UserModelDTO $user)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.creator = :user')->setParameter('user', $user);
        $qb->andWhere('t.state <> :state')->setParameter('state', Ticket::CLOSED);
        return $qb->getQuery()->execute();
    }

    /**
     * @param UserModelDTO $user
     * @return mixed
     */
    public function findAssignedTo(UserModelDTO $user)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.assignor = :user')->setParameter('user', $user);
        $qb->andWhere('t.state <> :state')->setParameter('state', Ticket::CLOSED);
        return $qb->getQuery()->execute();
    }

    /**
     * @param UserModelDTO $user
     * @return mixed
     */
    public function findClosedForUser(UserModelDTO $user)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->where('t.assignor = :user or t.creator = :user')->setParameter('user', $user);
        $qb->andWhere('t.state = :state')->setParameter('state', Ticket::CLOSED);

        return $qb->getQuery()->execute();
    }
}