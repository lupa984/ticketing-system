<?php
/**
 * Created by PhpStorm.
 * User: leggy
 * Date: 09/11/18
 * Time: 00:17
 */

namespace App\Repository;


use App\Madisoft\Entity\User;
use App\Model\DTO\UserModelDTO;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param UserModelDTO $dto
     * @return bool
     * @throws ORMException
     */
    public function save(UserModelDTO $dto)
    {
        $this->getEntityManager()->persist($dto);
        return true;
    }

    /**
     * @param $email
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByEmail($email)
    {
//        die($email);
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.email = :email')->setParameter('email', $email);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return mixed
     */
    public function getAdmins(?UserModelDTO $excluded = null)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.role = :role')->setParameter('role', User::ROLE_ADMIN);

        if($excluded){
            $qb->andWhere('u.email <> :email')->setParameter('email', $excluded->email);
        }

        return $qb->getQuery()->execute();
    }
}