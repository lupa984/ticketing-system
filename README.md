# ticketing system

simple example of a ticketing system

### steps

* clone the project
* run `composer install`
* configure `.env` file
    * complete DATABASE_URL with your local parameters
    * consider JWT_PASSPHRASE for the further steps
* run `bin/console doctrine:database:create`
* run `bin/console hautelook:fixtures:load`
* create folder `jwt` under confgi
* run `openssl genrsa -out config/jwt/private.pem -aes256 4096`
* run `openssl rsa -pubout -in config/jwt/private.pem -out config/jwt/public.pem`
* customers login provided:
    * `customer_1@customer.it` with password `customer_1`
    * `customer_2@customer.it` with password `customer_2`
* admin login provided:
    * `admin_1@madisoft.it` with password `admin_1`
    * `admin_2@madisoft.it` with password `admin_2`

### tests

brief phpspec suite to kick off the project domain core.  
run `bin/phpspec run`